One-Class Support Vector Machines
=================================

This [MLHub](https://mlhub.ai) package introduces and demonstrates the
concept of [one-class support vector
machines](https://onepager.togaware.com/One_Class_Support_Vector_Ma.html)


Visit the gitlab repository for more details:
<https://gitlab.com/kayontoga/ocsvm>

Quick Start Command Line Examples
---------------------------------

After installing the package:

```console
$ ml demo ocsvm
```

Usage
-----

- To install mlhub (Ubuntu):

```console
$ pip3 install mlhub
$ ml configure
```

- To install and configure the demo:

```console
$ ml install ocsvm
$ ml configure ocsvm
$ ml readme ocsvm
$ ml commands ocsvm
```

Demonstration
-------------

```console
================================
One Class Support Vector Machine
================================

A support vector machine identifies a hyperplane (or a line in 2D space)
which linearly separates the observations that belong to different
classes. Those observations that are closest to the hyperplane are
called the support vectors. The concept of a margin is used to
maximise the distance between the support vectors.

Data is not usually linearly separable. Thus some kind of mapping of
the data to higher dimensions is performed to find a space where it is
possible to separate the classes with a hyperplane. A so-called kernel
function is used to do this. A common one is the radial basis function.

This demonstration illustrates the concept of a one-class SVM. A one-class
model will be built using the iris dataset. The iris dataset has 3 classes:

    setosa versicolor  virginica 
        50         50         50 

The dataset contains 150 observations of 4 variables across the 3 classes.

Here is a random sample of the dataset:

   sepal_length sepal_width petal_length petal_width    species
1           4.9         3.6          1.4         0.1     setosa
2           7.4         2.8          6.1         1.9  virginica
3           5.0         3.0          1.6         0.2     setosa
4           5.0         3.4          1.6         0.4     setosa
5           6.9         3.1          4.9         1.5 versicolor
6           4.4         3.2          1.3         0.2     setosa
7           4.6         3.6          1.0         0.2     setosa
8           5.6         3.0          4.5         1.5 versicolor
9           5.1         3.7          1.5         0.4     setosa
10          6.5         3.0          5.8         2.2  virginica

Press Enter to continue: 

======================
Model Training Dataset
======================

Observations belonging to just one of the classes, "setosa", will be used to
train the one-class model. The model will then be applied to the full dataset to
evaluate how well it distinguishes between observations of that class and the
other two classes. Here's a random sample of the setosa observations.

   sepal_length sepal_width petal_length petal_width species
1           4.8         3.0          1.4         0.1  setosa
2           5.0         3.2          1.2         0.2  setosa
3           5.8         4.0          1.2         0.2  setosa
4           4.6         3.1          1.5         0.2  setosa
5           5.1         3.8          1.9         0.4  setosa
6           4.6         3.6          1.0         0.2  setosa
7           4.9         3.1          1.5         0.1  setosa
8           4.8         3.1          1.6         0.2  setosa
9           5.5         4.2          1.4         0.2  setosa
10          4.8         3.0          1.4         0.3  setosa

Press Enter to continue: 

=================
Model Performance
=================

The model is now applied to the full iris dataset. Because the full dataset
includes the observations from which the model was trained the performance
measures are an over-estimate of the true performance.

From the confusion matrix below we can see there are 39 true positives,
100 true negatives, and 0 false positive and 11 false negatives.

Overall the accuracy is 93%.

         Actual
Predicted other setosa
   other    100     11
   setosa     0     39

Press Enter to continue: 

====================
Identifying Outliers
====================

A one-class support vector machine can be used to identify outliers. Essentially
a model is fit to the data and then those observations that don't fit the model
so well are the outliers.

The following example was motived by the Data Tech Notes article of 8 Jan 2018:

    https://www.datatechnotes.com/2018/01/outlier-check-with-svm-novelty.html

Press Enter to continue: 

==============
Random Dataset
==============

A random sequence of numbers are generated, and then a much smaller random subset
of these (5) are replaced by other numbers that form the outliers. The
dataset is below. The outliers should be clear.

  [1]  2.48  3.26  3.27  1.96  6.30  6.98  5.33  2.59  0.81  7.02  6.68  3.15
 [13]  5.81  7.76  3.48  4.21  0.75 16.00  6.67  2.80  6.36  2.50 19.00  2.29
 [25]  2.21 20.00  4.73  7.90  5.56  0.68  1.72  4.72  9.92  0.43  5.24  8.08
 [37]  1.29  7.87 17.00  6.44  7.72  6.31  9.75  0.19  4.00  3.39  3.74  6.39
 [49]  3.29  8.85  1.25  9.52  2.65  1.59  8.20  4.68  4.22  8.27  1.12  4.81
 [61]  4.93  4.58  0.34  2.04  2.14  0.67  5.96  4.96  3.67  9.37  2.10  1.74
 [73]  2.65  6.84  2.04  9.99  6.32  6.72  5.53  6.54  2.67  4.35  2.32  4.80
 [85]  1.84  9.01  1.74  3.14  3.42 15.00  1.24  9.29  8.15  7.08  7.52  3.96
 [97]  3.09  5.28  5.79  3.06

They will be even more obvious when we plot them...

Press Enter to continue: 

Close the graphic window using Ctrl-w.
```
![](random_points.png)
```console
Press Enter to continue: 

===============
Build the Model
===============

A one-class model using a linear kernel function is built and applied to
the same dataset to predict if the point is within the model or not. Here the model
predicts the 5 outliers.

  model outlier 
     95       5 

Press Enter to continue: 

================
Plot of Outliers
================

The outliers are shown as red dots on the plot.

Close the graphic window using Ctrl-w.
```
![](random_points_outliers.png)
